﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadLevel : MonoBehaviour {

	public string level;

	public void LevelLoad(string level)
	{
		SceneManager.LoadScene(level);
	}
}
