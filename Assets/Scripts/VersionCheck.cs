﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VersionCheck : MonoBehaviour
{

	private string curVersion = "ALPHA 0.01";
	private string errorMessage = "Version Cannot load";
	public Text textVersion;

	void Start()
	{
		VersionChecker();
	}

	void VersionChecker()
	{
		if(textVersion != null)
		{
			textVersion.text = curVersion;
		}
		else{
			Debug.LogWarning("welcomeText not assigned");
			textVersion.text = errorMessage;

		}

	}
}